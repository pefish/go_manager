import FileUtil from 'p_js_common/lib/node/utils/file'
import OsUtil from 'p_js_common/lib/node/utils/os'
import ShellHelper from 'p_js_common/lib/node/helpers/shell'
import JsonFormatter from 'p_js_common/lib/node/utils/json_formatter'
import ErrorHelper from 'p_js_common/lib/node/helpers/error'
import GoUtil from './go_util'
import TimeUtil from 'p_js_common/lib/node/utils/time'
import os from 'os'
import FileWatcherHelper from 'p_js_common/lib/node/helpers/file_watcher'

export default class CommandHandler {

  constructor (shellHelper) {
    this._shellHelper = shellHelper
    this._currentPath = FileUtil.getWorkPath()
    this._modulePath = 'vendor'
  }

  _cloneRepo (name, targetPath, version) {
    logger.info(`${name}...`)
    const shellHelper = new ShellHelper(false)
    shellHelper.execSyncInSilent(`git clone https://${name} ${targetPath} --recurse-submodules --shallow-submodules`)
    if (!version) {
      shellHelper.execSyncInSilent(`cd ${targetPath} && git tag --list 'v*' --sort v:refname | tail -n 1`)
      if (shellHelper.result['stdout']) {
        version = shellHelper.result['stdout']
      } else {
        version = shellHelper.execSyncForResult(`cd ${targetPath} && git log -n 1 --pretty=format:"%H"`, {
          silent: true
        })['stdout']
      }
      version = version.removeLastEnter()
    }
    shellHelper.execSyncInSilent(`cd ${targetPath} && git checkout ${version}`)
    shellHelper.execSyncInSilent(`cd ${targetPath} && rm -rf _* && rm -rf ${[`.git`, `.github`, `.example`, `.test`, `examples`].join(` `)}`)
    shellHelper.execSyncInSilent(`cd ${targetPath} && find . -type f -name '*_test.go' -delete && find . -type f ! \\( -name "*.go" -o -name "*.s" \\) -delete`)
    logger.info(`${name} done!`)
    return version
  }

  async upgradeSubcommand (args) {
    try {
      if (this._runScripts(args, 'build')) {
        return
      }

      let name = args['_'][1]
      name = name.split('@')[0]
      const targetPath = `${this._currentPath}/src/${this._modulePath}/${name}`
      logger.info(`${name}...`)
      if (FileUtil.existsSync(targetPath)) {
        new ShellHelper(false).execSyncInSilent(`rm -rf ${targetPath}`)
      }
      logger.info(`${name} removed!`)
      await this.addSubcommand(args)
    } catch (err) {
      logger.error(`ERROR!!! ${err.getErrorMessage()}`)
    }
  }

  _parsePackageParam (str) {
    const result = {}

    const packageNameVersionAndFakeNameArr = str.split(':')
    const packageNameAndVersionStr = packageNameVersionAndFakeNameArr[0]
    const packageNameAndVersionArr = packageNameAndVersionStr.split('@')
    result['name'] = packageNameAndVersionArr[0]
    if (packageNameVersionAndFakeNameArr.length > 1) {
      result['fakeName'] = packageNameVersionAndFakeNameArr[1]
    } else {
      result['fakeName'] = result['name']
    }
    if (packageNameAndVersionArr.length > 1) {
      result['version'] = packageNameAndVersionArr[1]
    }

    return result
  }

  async addSubcommand(args) {
    try {
      let packageInfo = GoUtil.getPackageInfo(this._currentPath)
      if (this._runScripts(args, 'add', packageInfo)) {
        return
      }

      for (let i = 1; i < args['_'].length; i++) {
        let { name, version, fakeName } = this._parsePackageParam(args['_'][i])

        const targetPath = `${this._currentPath}/src/${this._modulePath}/${fakeName}`
        if (FileUtil.existsSync(targetPath)) {
          logger.info(`${name}...`)
          logger.info(`${name} done!`)
          return
        }

        version = this._cloneRepo(name, targetPath, version)

        if (args['dev'] === true) {
          !packageInfo['devDependencies'] && (packageInfo['devDependencies'] = {})
          if (fakeName === name) {
            packageInfo['devDependencies'][name] = version
          } else {
            packageInfo['devDependencies'][name] = {
              fake_name: fakeName,
              version: version
            }
          }
        } else {
          !packageInfo['dependencies'] && (packageInfo['dependencies'] = {})
          if (fakeName === name) {
            packageInfo['dependencies'][name] = version
          } else {
            packageInfo['dependencies'][name] = {
              fake_name: fakeName,
              version: version
            }
          }
        }
        FileUtil.writeSync(`${this._currentPath}/package.json`, JsonFormatter.format(JSON.stringify(packageInfo)))
      }
      logger.info('done!!!')
    } catch (err) {
      logger.error(`ERROR!!! ${err.getErrorMessage()}`)
    }
  }

  async removeSubcommand(args) {
    try {
      let packageInfo = GoUtil.getPackageInfo(this._currentPath)
      if (this._runScripts(args, 'remove', packageInfo)) {
        return
      }

      if (args['_'].length === 1) {
        throw new ErrorHelper(`params error`)
      }

      for (let i = 1; i < args['_'].length; i++) {
        let { name } = this._parsePackageParam(args['_'][i])
        let fakeName = name
        if (packageInfo['devDependencies'] && packageInfo['devDependencies'][name]) {
          fakeName = this._parsePackageDetail(name, packageInfo['devDependencies'][name])['fake_name']
          delete packageInfo['devDependencies'][name]
        }

        if (packageInfo['dependencies'] && packageInfo['dependencies'][name]) {
          fakeName = this._parsePackageDetail(name, packageInfo['dependencies'][name])['fake_name']
          delete packageInfo['dependencies'][name]
        }

        const shellHelper = new ShellHelper(false)
        shellHelper.execSync(`rm -rf ${this._currentPath}/src/${this._modulePath}/${fakeName}`)

        FileUtil.writeSync(`${this._currentPath}/package.json`, JsonFormatter.format(JSON.stringify(packageInfo)))

      }

      logger.info('done!!!')
    } catch (err) {
      logger.error(`ERROR!!! ${err.getErrorMessage()}`)
    }

  }

  _parsePackageDetail (name, packageDetail) {
    let result = {}

    if (packageDetail instanceof Object) {
      result['fake_name'] = packageDetail['fake_name'] || name
      if (!packageDetail['version']) {
        throw new ErrorHelper(`package ${name} loss of version`)
      }
      result['version'] = packageDetail['version']
    } else {
      result['fake_name'] = name
      result['version'] = packageDetail
    }
    return result
  }

  async installSubcommand(args) {
    try {
      let packageInfo = GoUtil.getPackageInfo(this._currentPath)
      if (this._runScripts(args, 'install', packageInfo)) {
        return
      }

      if (packageInfo['dependencies']) {
        for (let [name, packageDetail] of Object.entries(packageInfo['dependencies'])) {
          const parsedPackageDetail = this._parsePackageDetail(name, packageDetail)
          const targetPath = `${this._currentPath}/src/${this._modulePath}/${parsedPackageDetail['fake_name']}`
          if (FileUtil.existsSync(targetPath)) {
            logger.info(`${name}...`)
            logger.info(`${name} done!`)
            continue
          }
          this._cloneRepo(name, targetPath, parsedPackageDetail['version'])
        }
      }
      if (!args['production']) {
        if (packageInfo['devDependencies']) {
          for (let [name, branch] of Object.entries(packageInfo['devDependencies'])) {
            const targetPath = `${this._currentPath}/src/${this._modulePath}/${name}`
            this._cloneRepo(name, targetPath, branch)
          }
        }
      }

      logger.info('done!!!')
    } catch (err) {
      logger.error(`ERROR!!! ${err.getErrorMessage()}`)
    }
  }

  async initSubcommand(args) {
    const result = await OsUtil.getArgsFromConsole({
      properties: {
        name: {
          description: 'name',
          type: 'string',
          required: true,
          default: this._currentPath.split('/').getLastOne(),
          hidden: false,
        },
        version: {
          description: 'version',
          type: 'string',
          required: true,
          default: '1.0.0',
          hidden: false,
        },
        description: {
          description: 'description',
          type: 'string',
          required: false,
          default: '',
          hidden: false,
        },
        author: {
          description: 'author',
          type: 'string',
          required: false,
          default: '',
          hidden: false,
        }
      }
    }, '')


    let tempPath = `${this._currentPath}/src`
    const shellHelper = new ShellHelper(false)
    if (!FileUtil.existsSync(tempPath)) {
      shellHelper.execSync(`mkdir ${tempPath}`)
    }

    tempPath = `${this._currentPath}/src/tests`
    if (!FileUtil.existsSync(tempPath)) {
      shellHelper.execSync(`mkdir ${tempPath}`)
    }

    tempPath = `${this._currentPath}/src/main`
    if (!FileUtil.existsSync(tempPath)) {
      shellHelper.execSync(`mkdir ${tempPath}`)
      FileUtil.writeSync(`${tempPath}/main.go`, `package main

import (
  "log"
)

func main() {
  log.Println(\`Hello World\`)
}
`)
    }

    tempPath = `${this._currentPath}/package.json`
    if (!FileUtil.existsSync(tempPath)) {
      FileUtil.writeSync(tempPath, JsonFormatter.format(JSON.stringify({
        name: result['name'],
        version: result['version'],
        description: result['description'],
        author: result['author'],
        scripts: {
          start: "gom start src/main/main.go",
          build_start: "gom build && ./bin/main"
        }
      })))
    }

    logger.info('done!!!')
  }

  async defaultHandler(args) {
    try {
      if (args['_'] && args['_'].length === 0) {
        await this.installSubcommand(args)
      } else if (args['_'] && args['_'].length > 0) {
        const cmd = args['_'].getFirstOne()

        let packageInfo = GoUtil.getPackageInfo(this._currentPath)
        if (packageInfo['scripts'] && Object.keys(packageInfo['scripts']).includes(cmd)) {
          logger.info(`$ ${packageInfo['scripts'][cmd]}`)
          try {
            new ShellHelper(false).execSync(packageInfo['scripts'][cmd] + ' ' + args['_'].removeFirstOne().join(' '))
          } catch (err) {
            // logger.error(err)
          }
          logger.info('done!!!')
        } else {
          logger.error(`命令 ${cmd} 没找到`)
        }
      }  else {
        logger.error(`命令 ${cmd} 没找到`)
      }
    } catch (err) {
      logger.error(`ERROR!!! ${err.getErrorMessage()}`)
    }
  }



  async buildSubcommand (args) {
    try {
      if (this._runScripts(args, 'build')) {
        return
      }


      let buildCmd = `go install main`
      if (args['_'].length > 1) {
        buildCmd = `go install ${args['_'].removeFirstOne().join(' ')}`
      }

      if (args['os']) {
        buildCmd = `GOOS=${args['os']} ${buildCmd}`
      }
      if (args['arch']) {
        buildCmd = `GOARCH=${args['arch']} ${buildCmd}`
      }
      const shellHelper = new ShellHelper(false)
      shellHelper.execSync(this._wrap(buildCmd))
      logger.info('done!!!')
    } catch (err) {
      logger.error(`ERROR!!! ${err.getErrorMessage()}`)
    }
  }

  _wrap (cmd) {
    return `GOPATH=${process.cwd()} ${cmd}`
  }

  async formatSubcommand (args) {
    try {
      if (this._runScripts(args, 'format')) {
        return
      }

      const shellHelper = new ShellHelper(false)
      shellHelper.execSync(this._wrap(`gofmt -w src`))
      logger.info('done!!!')
    } catch (err) {
      logger.error(`ERROR!!! ${err.getErrorMessage()}`)
    }
  }

  async globalSubcommand (args) {
    try {
      if (args['_'].length === 1) {
        throw new ErrorHelper(`params error`)
      }
      if (args['_'][1] === 'add') {
        for (let i = 2; i < args['_'].length; i++) {
          let name = args['_'][i]
          logger.info(`${name}...`)
          const shellHelper = new ShellHelper(false)
          shellHelper.execSyncInSilent(`go get -u ${name} && source /etc/profile`)
          logger.info(`${name} done!`)
        }
        logger.info('done!!!')
      } else if (args['_'][1] === 'remove') {
        for (let i = 2; i < args['_'].length; i++) {
          let name = args['_'][i]
          const goPath = GoUtil.getGoPath()
          logger.info(`${name}...`)
          const shellHelper = new ShellHelper(false)
          shellHelper
            .execSyncInSilent(`go clean -i ${name} && source /etc/profile`)
            .execSyncInSilent(`rm -rf ${goPath}/src/${name}`)
            .execSyncInSilent(`rm -rf ${goPath}/pkg/${os.platform() + (os.arch() === 'x64' ? '_amd64' : '')}/${name}`)
          logger.info(`${name} done!`)
        }
        logger.info('done!!!')
      } else {
        throw new ErrorHelper(`subcommand not found!`)
      }
    } catch (err) {
      logger.error(`ERROR!!! ${err.getErrorMessage()}`)
    }
  }

  async testSubcommand (args) {
    try {
      if (this._runScripts(args, 'test')) {
        return
      }

      if (args['_'].length < 2) {
        throw new ErrorHelper(`请指定测试包名`)
      }
      let packageName = args['_'][1]
      let shell_ = `go test -v -cover=true ${packageName}`
      if (args['run']) {
        shell_ += ` -run ${args['run']}`
      }
      new ShellHelper(false).execSync(this._wrap(shell_))

      logger.info('done!!!')
    } catch (err) {
      logger.error(`ERROR!!! ${err.getErrorMessage()}`)
    }
  }

  _runScripts (args, cmd, packageInfo = null) {
    !packageInfo && (packageInfo = GoUtil.getPackageInfo(this._currentPath))
    if (packageInfo['scripts'] && Object.keys(packageInfo['scripts']).includes(cmd)) {
      logger.info(`$ ${packageInfo['scripts'][cmd]}`)
      try {
        new ShellHelper(false).execSync(packageInfo['scripts'][cmd] + ' ' + args['_'].removeFirstOne().join(' '))
      } catch (err) {

      }
      logger.info('done!!!')
      return true
    } else {
      return false
    }
  }

  async wrapSubcommand (args) {
    try {
      if (this._runScripts(args, 'wrap')) {
        return
      }

      if (args['_'].length < 2) {
        throw new ErrorHelper(`请指定要执行的命令`)
      }
      try {
        const cmd = this._wrap(args['_'][1])
        logger.info(`$ ${cmd}`)
        new ShellHelper(false).execSync(cmd)
      } catch (err) {

      }
      logger.info('done!!!')
    } catch (err) {
      logger.error(`ERROR!!! ${err.getErrorMessage()}`)
    }
  }

  async startSubcommand (args) {
    try {
      let packageInfo = GoUtil.getPackageInfo(this._currentPath)
      if (this._runScripts(args, 'start', packageInfo)) {
        return
      }

      const runCmd = this._wrap(`go run` + ' ' + args['_'].removeFirstOne().join(' '))
      logger.info(`$ ${runCmd}`)
      this._shellHelper.execAsyncDetach(runCmd)
      let reloading = false
      let watcher = new FileWatcherHelper()

      async function restart() {
        if (!reloading) {
          reloading = true
          logger.info(`A change has been detected, reloading now...`)
          this._shellHelper.cleanAllDetachedProcess()
          await TimeUtil.sleep(500)
          this._shellHelper.execAsyncDetach(runCmd)
          reloading = false
        }
      }

      watcher.watch(this._currentPath, async (event, name) => {
        try {
          await restart.bind(this)()
        } catch (err) {
          logger.error(err)
        }
      }, [
        'src/vendor',
        'bin',
        '.idea',
        '.git',
        'tests',
        'test'
      ].append(packageInfo['gom'] && packageInfo['gom']['reload_ignore'] ? packageInfo['gom']['reload_ignore'] : []))

      // 只是监听主进程
      // const keyPressListener = new KeyPressListener()
      // keyPressListener.start()
      // keyPressListener.onKeyPressAfterEnter(async (keys, str) => {
      //   try {
      //     if (str === 'rs') {
      //       await restart.bind(this)()
      //     }
      //   } catch (err) {
      //     logger.error(err)
      //   }
      // })
    } catch (err) {
      logger.error(`ERROR!!! ${err.getErrorMessage()}`)
    }
  }

  getHandler (name) {
    if (name === 'default') {
      return this.defaultHandler
    } else {
      return this[`${name}Subcommand`]
    }
  }
}
