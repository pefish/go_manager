import 'node-assist'
import CommonUtil from 'p_js_common/lib/node/utils/common'
import parseParams from 'p_js_common/lib/node/plugins/parse_params'
import CommandHandler from './command_handler'
import ShellHelper from 'p_js_common/lib/node/helpers/shell'

async function clean() {
  try {
    shellHelper && shellHelper.cleanAllDetachedProcess()
  } catch (err) {

  }
}

async function stop() {
  await clean()
  logger.error(`应用已经停止`)
}

async function exit() {
  await clean()
  process.exit(0)
}

process.on('SIGINT', async () => {
  await exit()
})

CommonUtil.startAsync(async () => {
  global['shellHelper'] = new ShellHelper(false)
  const handler = new CommandHandler(shellHelper)

  parseParams({
    usage: 'Usage: gom [command] [options]',
    command: {
      ' ': {
        desc: '同于 gom install',
        option: {

        }
      },
      '<scripts>': {
        desc: '执行package中的命令',
        option: {

        }
      },
      add: {
        desc: '安装三方库',
        option: {
          dev: {
            alias: 'dev',
            description: '开发环境依赖'
          }
        },
        handler: handler.getHandler('add').bind(handler)
      },
      remove: {
        desc: '移除三方库',
        option: {

        },
        handler: handler.getHandler('remove').bind(handler)
      },
      install: {
        desc: '下载安装所有三方库',
        option: {
          production: {
            alias: 'production',
            description: '除了开发环境的依赖'
          }
        },
        handler: handler.getHandler('install').bind(handler)
      },
      init: {
        desc: '初始化go项目',
        option: {

        },
        handler: handler.getHandler('init').bind(handler)
      },
      build: {
        desc: '构建go项目',
        option: {
          os: {
            alias: 'os',
            description: '编译成啥系统的支持格式'
          },
          arch: {
            alias: 'arch',
            description: '编译成啥架构'
          }
        },
        handler: handler.getHandler('build').bind(handler)
      },
      format: {
        desc: '格式化go代码',
        option: {

        },
        handler: handler.getHandler('format').bind(handler)
      },
      upgrade: {
        desc: '修改三方库版本',
        option: {

        },
        handler: handler.getHandler('upgrade').bind(handler)
      },
      global: {
        desc: '全局命令',
        option: {

        },
        handler: handler.getHandler('global').bind(handler)
      },
      start: {
        desc: '热启动。代码有改变保存立刻重启',
        option: {

        },
        handler: handler.getHandler('start').bind(handler)
      },
      test: {
        desc: '单元测试',
        option: {
          run: {
            alias: 'run',
            description: '指定只运行哪个测试方法'
          }
        },
        handler: handler.getHandler('test').bind(handler)
      },
      wrap: {
        desc: '在当前项目下执行命令',
        option: {

        },
        handler: handler.getHandler('wrap').bind(handler)
      },
    },
    handler: handler.getHandler('default').bind(handler)
  })
}, exit, false, false)
