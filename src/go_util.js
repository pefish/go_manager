import ShellHelper from 'p_js_common/lib/node/helpers/shell'
import FileUtil from 'p_js_common/lib/node/utils/file'

export default class GoUtil {
  static getGoPath () {
    const shellHelper = new ShellHelper(false)
    shellHelper.execSyncInSilent(`echo $GOPATH`)
    shellHelper.result = shellHelper.result.removeLastEnter()
    return shellHelper.result['stdout'] || '~/go'
  }

  static getPackageInfo (currentPath) {
    let datas = {}, packageFilePath = `${currentPath}/package.json`
    if (FileUtil.existsSync(packageFilePath)) {
      datas = FileUtil.loadFromJsonFile(packageFilePath)
    }
    return datas
  }
}
