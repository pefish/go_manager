# go_manager

#### 项目介绍
go版本管理工具(思路源于yarn)

1. 熟悉npm或者yarn的，零学习成本

2. go的各个项目之间完全隔离

3. 三方依赖版本管理

#### 安装

1. yarn global add go_manager

#### 帮助文档

1. gom --help

#### 使用说明

1. (安装依赖) gom add github.com/kataras/iris@v1.0 

2. (卸载依赖) gom remove github.com/kataras/iris 
     
3. (安装package.json中所有依赖) gom install                      

4. (安装开发环境依赖, 用gitee.com/kataras/iris替代路径) gom add github.com/kataras/iris:gitee.com/kataras/iris master --dev   

5. (安装package.json中除了开发环境依赖的所有依赖) gom install --production    

6. (更新三方库版本) gom upgrade github.com/kataras/iris@latest

7. (与 gom install 一致) gom                                

8. (初始化go项目) gom init                                  

9. (执行package.json中的命令) gom <package.json中scripts中的命令>

10. (格式化go代码) gom format

11. (安装全局包) gom global add github.com/codegangsta/gin

12. (卸载全局包) gom global remove github.com/codegangsta/gin

13. (热启动) gom start src/main/main.go

14. (单元测试,测试utils包中的Haha方法) gom test utils --run TestHaha

15. 输入rs，按回车，自动reload(注意：只适用于直接gom start {..}启动的)

16. (编译。不指定--os和--arch则自动编译为操作系统支持的格式) gom build --os linux --arch 386

17. (在当前项目下执行命令) gom wrap "go install main"


所有三方库存放在 src/vendor 中
