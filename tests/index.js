import OsUtil from 'p_js_common/lib/node/utils/os'
import CommonUtil from 'p_js_common/lib/node/utils/common'

OsUtil.getArgsFromConsole({
  properties: {
    name: {
      description: 'name',
      type: 'string',
      required: true,
      hidden: false,
    }
  }
}, '').then((d) => {
  console.log(d)
})

CommonUtil.startAsync(async () => {

})
