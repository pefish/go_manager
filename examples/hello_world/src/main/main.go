package main

import (
	"database/sql"
	"global"
	_ "go_modules/github.com/go-sql-driver/mysql"
	"log"
)

func main() {
	// 销毁函数
	defer func() {
		if global.DbHelper != nil {
			global.DbHelper.Close()
		}
	}()

	// 初始化数据库连接
	db, err := sql.Open("mysql", "root:root@tcp(127.0.0.1:3306)/test?charset=utf8")
	if err != nil {
		log.Fatal(err)
	}
	global.DbHelper = db

	log.Println(`haha`)
}
